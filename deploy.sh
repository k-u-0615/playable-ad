TARGET="kito/test"
TARGET_PATH="/home/aquacp/www/aquacp.com/$TARGET"

sshpass -p $(cat ~/u_aqua.json | jq -r .pass) \
    scp ./index.html "aquacp@aquacp.aqua-web.co.jp:$TARGET_PATH/index.html"

echo "success!"
echo http://u.aqua-web.co.jp/kito/test